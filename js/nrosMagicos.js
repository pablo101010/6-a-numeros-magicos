// Números mágicos
// Crea una web con bootstrap y js, que contenga un botón comenzar el juego, en ese momento se crea un número aleatorio que el usuario deberá adivinar, la interfaz del usuario debe tener además un input para ingresar un número y un botón enviar, al presionar el botón enviar mostrar en un alert si el usuario adivino o no el número mágico, si no lo adivino indicarle con un alert si el numero que ingreso es mayor o menor al número mágico.
// Cuando el usuario adivine el numero mostrar un mensaje indicando al usuario que adivino el numero.


Init();


function Init() {

   var comJuego = document.getElementById("comenzar");
   var enviar = document.getElementById("btnEnviar");
   var entrada = document.getElementById("casillaEntrada");
   var nroAleatorio;


   comJuego.onclick = function (e) {

      //console.log("comience el juego"); 
      nroAleatorio = Math.round(Math.random() * (101 - 1) + 1);
      console.log(nroAleatorio);
      entrada.value = "";
   }

   enviar.onclick = function (e) {

      console.log("enviar");
      if (entrada.value == nroAleatorio) {
         console.log("correcto");
         alert("GANASTE!!! Felicitaciones!!!");
      }
      if (entrada.value < nroAleatorio) {
         alert("MAS");
      }
      if (entrada.value > nroAleatorio) {
         alert("MENOS");
      }

   }
}







